package com.sensorcon.dataloggerutilities;

import com.j256.ormlite.field.DatabaseField;

public class ProfileEntry {
	
	@DatabaseField(generatedId = true)
	int id;
	@DatabaseField(index = true)
	String name;
	@DatabaseField
	boolean ambientTempEnabled;
	@DatabaseField
	boolean humidityEnabled;
	@DatabaseField
	boolean pressureEnabled;
	@DatabaseField
	boolean precisionGasEnabled;
	@DatabaseField
	boolean reducingGasEnabled;
	@DatabaseField
	boolean oxidizingGasEnabled;
	@DatabaseField
	boolean irTempEnabled;
	@DatabaseField
	boolean capacitanceEnabled;
	@DatabaseField
	boolean externalADCEnabled;
	@DatabaseField
	boolean redEnabled;
	@DatabaseField
	boolean greenEnabled;
	@DatabaseField
	boolean blueEnabled;
	@DatabaseField
	boolean clearEnabled;
	@DatabaseField
	int rate;
	@DatabaseField
	long time;
	
	public ProfileEntry() {}
	
	public ProfileEntry(String name, boolean ambientTempEnabled, boolean humidityEnabled, boolean pressureEnabled, boolean precisionGasEnabled, boolean reducingGasEnabled, boolean oxidizingGasEnabled, boolean irTempEnabled, boolean capacitanceEnabled, boolean externalADCEnabled, boolean redEnabled, boolean greenEnabled, boolean blueEnabled, boolean clearEnabled, int rate, long time) {
		this.name = name;
		this.ambientTempEnabled = ambientTempEnabled;
		this.humidityEnabled = humidityEnabled;
		this.pressureEnabled = pressureEnabled;
		this.precisionGasEnabled = precisionGasEnabled;
		this.reducingGasEnabled = reducingGasEnabled;
		this.oxidizingGasEnabled = oxidizingGasEnabled;
		this.irTempEnabled = irTempEnabled;
		this.capacitanceEnabled = capacitanceEnabled;
		this.externalADCEnabled = externalADCEnabled;
		this.redEnabled = redEnabled;
		this.greenEnabled = greenEnabled;
		this.blueEnabled = blueEnabled;
		this.clearEnabled = clearEnabled;
		this.rate = rate;
		this.time = time;
	}
	
	public String getName() {
		return name;
	}
	public boolean getAmbientTempEnabled() {
		return ambientTempEnabled;
	}
	public boolean getHumidityEnabled() {
		return humidityEnabled;
	}
	public boolean getPressureEnabled() {
		return pressureEnabled;
	}
	public boolean getPrecisionGasEnabled() {
		return precisionGasEnabled;
	}
	public boolean getReducingGasEnabled() {
		return reducingGasEnabled;
	}
	public boolean getOxidizingGasEnabled() {
		return oxidizingGasEnabled;
	}
	public boolean getIrTempEnabled() {
		return irTempEnabled;
	}
	public boolean getCapacitanceEnabled() {
		return capacitanceEnabled;
	}
	public boolean getExternalADCEnabled() {
		return externalADCEnabled;
	}
	public boolean getRedEnabled() {
		return redEnabled;
	}
	public boolean getGreenEnabled() {
		return greenEnabled;
	}
	public boolean getBlueEnabled() {
		return blueEnabled;
	}
	public boolean getClearEnabled() {
		return clearEnabled;
	}
	public int getRate() {
		return rate;
	}
	public long getTime() {
		return time;
	}
}
