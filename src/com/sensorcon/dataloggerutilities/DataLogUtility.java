package com.sensorcon.dataloggerutilities;

import android.util.Log;

public class DataLogUtility {
	
	public byte[] createInitPacket() {
		byte packet[] = new byte[16];
		
		packet[0] = (byte)0x50;
		packet[1] = (byte)0x0E;
		packet[2] = (byte)0x50;

		byte sensorMSB = 0x00;
		byte sensorLSB = 0x00;
		
		if(DataLogConfiguration.sensorsEnabled[0] == true) {
			sensorLSB |= 0x01;
		}
		if(DataLogConfiguration.sensorsEnabled[1] == true) {
			sensorLSB |= 0x02;
		}
		if(DataLogConfiguration.sensorsEnabled[2] == true) {
			sensorLSB |= 0x04;
		}
		if(DataLogConfiguration.sensorsEnabled[3] == true) {
			sensorLSB |= 0x08;
		}
		if(DataLogConfiguration.sensorsEnabled[4] == true) {
			sensorLSB |= 0x10;
		}
		if(DataLogConfiguration.sensorsEnabled[5] == true) {
			sensorLSB |= 0x20;
		}
		if(DataLogConfiguration.sensorsEnabled[6] == true) {
			sensorLSB |= 0x40;
		}
		if(DataLogConfiguration.sensorsEnabled[7] == true) {
			sensorLSB |= 0x80;
		}
		if(DataLogConfiguration.sensorsEnabled[8] == true) {
			sensorMSB |= 0x01;
		}
		if(DataLogConfiguration.sensorsEnabled[9] == true) {
			sensorMSB |= 0x02;
		}
		if(DataLogConfiguration.sensorsEnabled[10] == true) {
			sensorMSB |= 0x04;
		}
		if(DataLogConfiguration.sensorsEnabled[11] == true) {
			sensorMSB |= 0x08;
		}
		if(DataLogConfiguration.sensorsEnabled[12] == true) {
			sensorMSB |= 0x10;
		}
		if(DataLogConfiguration.sensorsEnabled[13] == true) {
			sensorMSB |= 0x20;
		}
		
		packet[3] = sensorMSB;
		packet[4] = sensorLSB;
		
		byte intervalMSB = 0x00;
		byte intervalLSB = 0x00;
		
		intervalLSB = (byte)(DataLogConfiguration.sampleRate & 0x000000FF);
		intervalMSB = (byte)((DataLogConfiguration.sampleRate >> 8) & 0x000000FF);
		
		packet[5] = intervalMSB;
		packet[6] = intervalLSB;
		
		byte timestamp3 = 0x00;
		byte timestamp2 = 0x00;
		byte timestamp1 = 0x00;
		byte timestamp0 = 0x00;
		
		DataLogConfiguration.timeStamp = System.currentTimeMillis() / 1000;
		Log.d("chris","Timestamp: " + DataLogConfiguration.timeStamp);
		timestamp0 = (byte)(DataLogConfiguration.timeStamp & 0x00000000000000FF);
		timestamp1 = (byte)((DataLogConfiguration.timeStamp >> 8) & 0x00000000000000FF);
		timestamp2 = (byte)((DataLogConfiguration.timeStamp >> 16) & 0x00000000000000FF);
		timestamp3 = (byte)((DataLogConfiguration.timeStamp >> 24) & 0x00000000000000FF);
		
		packet[7] = timestamp3;
		packet[8] = timestamp2;
		packet[9] = timestamp1;
		packet[10] = timestamp0;
		
		byte totalLogs3 = 0x00;
		byte totalLogs2 = 0x00;
		byte totalLogs1 = 0x00;
		byte totalLogs0 = 0x00;
		
		totalLogs0 = (byte)(DataLogConfiguration.totalLogs & 0x00000000000000FF);
		totalLogs1 = (byte)((DataLogConfiguration.totalLogs >> 8) & 0x00000000000000FF);
		totalLogs2 = (byte)((DataLogConfiguration.totalLogs >> 16) & 0x00000000000000FF);
		totalLogs3 = (byte)((DataLogConfiguration.totalLogs >> 24) & 0x00000000000000FF);
		
		packet[11] = totalLogs3;
		packet[12] = totalLogs2;
		packet[13] = totalLogs1;
		packet[14] = totalLogs0;
		
		packet[15] = 0x00;
		
		Log.d("chris","["+String.format("%02X", packet[0])+"]["+String.format("%02X", packet[1])+"]["+String.format("%02X", packet[2])+"]["+String.format("%02X", packet[3])+"]["+String.format("%02X", packet[4])+"]["+String.format("%02X", packet[5])+"]["+String.format("%02X", packet[6])+"]["+String.format("%02X", packet[7])+"]["+String.format("%02X", packet[8])+"]["+String.format("%02X", packet[9])+"]["+String.format("%02X", packet[10])+"]["+String.format("%02X", packet[11])+"]["+String.format("%02X", packet[12])+"]["+String.format("%02X", packet[13])+"]["+String.format("%02X", packet[14])+"]["+String.format("%02X", packet[15])+"]");
		
		return packet;
	}

}
