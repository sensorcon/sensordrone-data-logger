package com.sensorcon.dataloggerutilities;

import java.sql.SQLException;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.sensorcon.sensordronedatalogger.R;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
	
	private static final String DATABASE_NAME = "dataLog.db";
	private static final int DATABASE_VERSION = 6;
	
	private Dao<DatabaseEntry, Integer> myDao = null;
	private RuntimeExceptionDao<DatabaseEntry, Integer> runtimeDao = null;
	
	private Dao<ProfileEntry, Integer> profileDao = null;
	private RuntimeExceptionDao<ProfileEntry, Integer> profileRuntimeDao = null;
	
	private Dao<MACEntry, Integer> MACDao = null;
	private RuntimeExceptionDao<MACEntry, Integer> MACRuntimeDao = null;

	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
		try {
			TableUtils.createTable(connectionSource, DatabaseEntry.class);
			TableUtils.createTable(connectionSource, ProfileEntry.class);
			TableUtils.createTable(connectionSource, MACEntry.class);
		} catch (SQLException e) {
			Log.e(DatabaseHelper.class.getName(), "Can't create database", e);
			throw new RuntimeException(e);
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
		try {
			Log.i(DatabaseHelper.class.getName(), "onUpgrade");
			TableUtils.dropTable(connectionSource, DatabaseEntry.class, true);
			TableUtils.dropTable(connectionSource, ProfileEntry.class, true);
			TableUtils.dropTable(connectionSource, MACEntry.class, true);
			
			onCreate(db, connectionSource);
		} catch (SQLException e) {
			Log.e(DatabaseHelper.class.getName(), "Can't drop databases", e);
			throw new RuntimeException(e);
		}		
	}
	
	public Dao<DatabaseEntry, Integer> getEntryDao() throws SQLException {
		if (myDao == null) {
			myDao = getDao(DatabaseEntry.class);
		}
		return myDao;
	}

	public RuntimeExceptionDao<DatabaseEntry, Integer> getEntryRuntimeDao() {
		if (runtimeDao == null) {
			runtimeDao = getRuntimeExceptionDao(DatabaseEntry.class);
		}
		return runtimeDao;
	}
	
	public Dao<ProfileEntry, Integer> getProfileDao() throws SQLException {
        if (profileDao == null) {
            profileDao = getDao(ProfileEntry.class);
        }
        return profileDao;
    }

    public RuntimeExceptionDao<ProfileEntry, Integer> getProfileRuntimeDao() {
        if (profileRuntimeDao == null) {
            profileRuntimeDao = getRuntimeExceptionDao(ProfileEntry.class);
        }
        return profileRuntimeDao;
    }
    
    public Dao<MACEntry, Integer> getMACDao() throws SQLException {
        if (MACDao == null) {
            MACDao = getDao(MACEntry.class);
        }
        return MACDao;
    }

    public RuntimeExceptionDao<MACEntry, Integer> getMACRuntimeDao() {
        if (MACRuntimeDao == null) {
            MACRuntimeDao = getRuntimeExceptionDao(MACEntry.class);
        }
        return MACRuntimeDao;
    }
	
	/**
	 * Close the database connections and clear any cached DAOs.
	 */
	@Override
	public void close() {
		super.close();
		runtimeDao = null;
	}
}
