package com.sensorcon.sensordronedatalogger;

import com.j256.ormlite.android.apptools.OrmLiteBaseActivity;
import com.sensorcon.dataloggerutilities.DataLogConfiguration;
import com.sensorcon.dataloggerutilities.DatabaseHelper;
import com.sensorcon.dataloggerutilities.ProfileEntry;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

public class Started extends OrmLiteBaseActivity<DatabaseHelper> {
	
	private ImageButton btnDone;
	private ImageButton btnSaveConfiguration;
	private TextView tvSensorsEnabled;
	private TextView tvSampleInterval;
	private TextView tvTotalTime;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.started);
		
		btnDone = (ImageButton)findViewById(R.id.btnDone);
		btnSaveConfiguration = (ImageButton)findViewById(R.id.btnSaveConfiguration);
		tvSensorsEnabled = (TextView)findViewById(R.id.tvSensorsEnabled);
		tvSampleInterval = (TextView)findViewById(R.id.tvSampleInterval);
		tvTotalTime = (TextView)findViewById(R.id.tvTotalTime);
		
		tvSensorsEnabled.setText(enabledSensorsText());
		tvSampleInterval.setText("Sample Interval: " + sampleIntervalText());
		tvTotalTime.setText("Total Time: " + totalTimeText());
		
		btnSaveConfiguration.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	save();
             }
        });
		
		btnDone.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	done();
             }
        });
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		switch(item.getItemId()){
		
		case R.id.instructions:
			
			Intent myIntent = new Intent(getApplicationContext(), Instructions.class);
			startActivity(myIntent);
			break;
		}
		return true;
	}

	private void save() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		
		LayoutInflater inflater = this.getLayoutInflater();
		
		View v = inflater.inflate(R.layout.save_configuration, null);
		
		final EditText input = (EditText)v.findViewById(R.id.configEditText);
		
		builder.setView(v).setMessage("Configuration Name")
		.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
            	String s = input.getText().toString();
            	
            	ProfileEntry entry = new ProfileEntry(	s,
            											DataLogConfiguration.sensorsEnabled[0],
            											DataLogConfiguration.sensorsEnabled[1],
            											DataLogConfiguration.sensorsEnabled[2],
            											DataLogConfiguration.sensorsEnabled[3],
            											DataLogConfiguration.sensorsEnabled[4],
            											DataLogConfiguration.sensorsEnabled[5],
            											DataLogConfiguration.sensorsEnabled[6],
            											DataLogConfiguration.sensorsEnabled[7],
            											DataLogConfiguration.sensorsEnabled[8],
            											DataLogConfiguration.sensorsEnabled[9],
            											DataLogConfiguration.sensorsEnabled[10],
            											DataLogConfiguration.sensorsEnabled[11],
            											DataLogConfiguration.sensorsEnabled[12],
            											DataLogConfiguration.sampleRate,
            											DataLogConfiguration.totalTime);
            	
            	getHelper().getProfileRuntimeDao().createIfNotExists(entry);
            	done();
            }
        })
        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                
            }
        });  
		
		final AlertDialog dialog = builder.create();
		dialog.show();
	}
	
	private void done() {
		kill();
	}
	
	private void kill() {
		this.finish();
	}
	
	private String enabledSensorsText() {
		String s = "";
		
		boolean first = true;
		
		if(DataLogConfiguration.sensorsEnabled[0] == true) {
			if(first == true) {
				first = false;
			}
			else {
				s += ", ";
			}
			
			s += "Ambient Temperature";
		}
		if(DataLogConfiguration.sensorsEnabled[1] == true) {
			if(first == true) {
				first = false;
			}
			else {
				s += ", ";
			}
			
			s += "Humidity";
		}
		if(DataLogConfiguration.sensorsEnabled[2] == true) {
			if(first == true) {
				first = false;
			}
			else {
				s += ", ";
			}
			
			s += "Pressure";
		}
		if(DataLogConfiguration.sensorsEnabled[3] == true) {
			if(first == true) {
				first = false;
			}
			else {
				s += ", ";
			}
			
			s += "Precision Gas";
		}
		if(DataLogConfiguration.sensorsEnabled[4] == true) {
			if(first == true) {
				first = false;
			}
			else {
				s += ", ";
			}
			
			s += "Reducing Gas";
		}
		if(DataLogConfiguration.sensorsEnabled[5] == true) {
			if(first == true) {
				first = false;
			}
			else {
				s += ", ";
			}
			
			s += "Oxidizing Gas";
		}
		if(DataLogConfiguration.sensorsEnabled[6] == true) {
			if(first == true) {
				first = false;
			}
			else {
				s += ", ";
			}
			
			s += "IR Temperature";
		}
		if(DataLogConfiguration.sensorsEnabled[7] == true) {
			if(first == true) {
				first = false;
			}
			else {
				s += ", ";
			}
			
			s += "Capacitance";
		}
		if(DataLogConfiguration.sensorsEnabled[8] == true) {
			if(first == true) {
				first = false;
			}
			else {
				s += ", ";
			}
			
			s += "External ADC";
		}
		if(DataLogConfiguration.sensorsEnabled[9] == true) {
			if(first == true) {
				first = false;
			}
			else {
				s += ", ";
			}
			
			s += "Red Filter";
		}
		if(DataLogConfiguration.sensorsEnabled[10] == true) {
			if(first == true) {
				first = false;
			}
			else {
				s += ", ";
			}
			
			s += "Green Filter";
		}
		if(DataLogConfiguration.sensorsEnabled[11] == true) {
			if(first == true) {
				first = false;
			}
			else {
				s += ", ";
			}
			
			s += "Blue Filter";
		}
		if(DataLogConfiguration.sensorsEnabled[12] == true) {
			if(first == true) {
				first = false;
			}
			else {
				s += ", ";
			}
			
			s += "Clear Filter";
		}
		if(DataLogConfiguration.sensorsEnabled[13] == true) {
			if(first == true) {
				first = false;
			}
			else {
				s += ", ";
			}
			
			s += "External Module";
		}
		
		return s;
	}
	
	private String sampleIntervalText() {
		String s = "";
		
		int interval = DataLogConfiguration.sampleRate;
		
		if((interval >= 60) && (interval < 3600)) {
			s = Integer.toString(interval / 60) + " Minute(s)";
		}
		else if(interval == 3600) {
			s = "1 Hour";
		}
		else if(interval < 60) {
			s = Integer.toString(interval) + " Second(s)";
		}
		
		return s;
	}
	
	private String totalTimeText() {
		String s = "";
		
		long time = DataLogConfiguration.totalTime;
		
		if(time >= 60) {
			s = Long.toString(time / 60) + " Hour(s)";
		}
		else {
			s = Long.toString(time) + " Minute(s)";
		}
		
		return s;
	}
}