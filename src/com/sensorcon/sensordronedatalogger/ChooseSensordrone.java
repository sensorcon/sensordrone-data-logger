package com.sensorcon.sensordronedatalogger;

import java.sql.SQLException;
import java.util.List;

import com.j256.ormlite.dao.Dao;
import com.sensorcon.dataloggerutilities.DatabaseHelper;
import com.sensorcon.dataloggerutilities.MACEntry;
import com.sensorcon.dataloggerutilities.ProfileEntry;
import com.sensorcon.dataloggerutilities.ViewConfiguration;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Lets user choose Sensordrone that they would like to view data from
 * 
 * @author Sensorcon, Inc.
 * @version 1.0.0 
 */
public class ChooseSensordrone extends Activity {
	
	ListView droneList;
	DroneAdapter droneAdapter;
	private ImageButton btnBack;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.choose_sensordrone);
		
		btnBack = (ImageButton)findViewById(R.id.btnBack);
		btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	back();
             }
        });
		
		// Get list of Sensordrones
		List<MACEntry> list = null;
		final DatabaseHelper helper = new DatabaseHelper(this);
		try {
			Dao<MACEntry,Integer> macDao = helper.getMACDao();
			list = macDao.queryForAll();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		final MACEntry[] macAddrs = (MACEntry[])list.toArray(new MACEntry[list.size()]);
		
		// Set up adapter for list view
		droneAdapter = new DroneAdapter(getApplicationContext(), macAddrs);
		droneList = (ListView)findViewById(R.id.sensordroneList);
		droneList.setAdapter(droneAdapter);

		droneList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, final View view,
					int position, long id) {

				// Store MAC
				ViewConfiguration.droneMAC = macAddrs[position].getMAC();

				// Start choose sensor activity
				Intent myIntent = new Intent(getApplicationContext(), ChooseSensor.class);
				startActivity(myIntent);
			}

		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		switch(item.getItemId()){
		
		case R.id.instructions:
			
			Intent myIntent = new Intent(getApplicationContext(), Instructions.class);
			startActivity(myIntent);
			break;
		}
		return true;
	}
	
	/**
	 * Kill activity when back button pressed
	 */
	private void back() {
		this.finish();
	}
	
	/**
	 * Adapter for showing drones
	 */
	public class DroneAdapter extends ArrayAdapter<MACEntry> {
		
		private final MACEntry[] macAddrList;
		private final Context context;
		
		public DroneAdapter(Context context, MACEntry[] macAddrArray) {
			super(context, R.layout.drone_row, macAddrArray);
			this.macAddrList = macAddrArray;
			this.context = context;
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			
			LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View rowView = inflater.inflate(R.layout.drone_row, parent, false);
			TextView rowTV = (TextView)rowView.findViewById(R.id.tvDrone);
			rowTV.setBackgroundResource(R.drawable.rounded);
			rowTV.setTextColor(Color.BLACK);
			
			
			String macAddr = macAddrList[position].getMAC();

			rowTV.setText(macAddr);
			
			return rowView;
		}
		
	}
}
