package com.sensorcon.sensordronedatalogger;

import com.sensorcon.dataloggerutilities.DataLogConfiguration;
import com.sensorcon.dataloggerutilities.DataLogUtility;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SimpleCursorAdapter;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.TextView;

public class TotalTime extends Activity {
	
	private ImageButton btnBack;
	private ImageButton btnNext;
	private ImageButton btnInfo;
	private EditText input;
	private RadioButton rbMinutes;
	private RadioButton rbHours;
	private TextView tvBattTime;
	private TextView tvPluggedTime;
	private TextView tvOr;
	
	private boolean minutes = true;
	private int time = 0;
	private long totalLogs = 0;
	int totalTimeBatt = 0;
	int totalTime = 0;
	
	private static final String TAG = "chris";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.total_time);
		
		btnBack = (ImageButton)findViewById(R.id.btnBack);
		btnNext = (ImageButton)findViewById(R.id.btnNext);
		btnInfo = (ImageButton)findViewById(R.id.btnInfo);
		input = (EditText)findViewById(R.id.enterMinutes);
		rbMinutes = (RadioButton)findViewById(R.id.minutes);
		rbHours = (RadioButton)findViewById(R.id.hours);
		tvBattTime = (TextView)findViewById(R.id.tv_battTime);
		tvPluggedTime = (TextView)findViewById(R.id.tv_pluggedTime);
		tvOr = (TextView)findViewById(R.id.tv_or);
		
		btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	back();
             }
        });
		
		btnNext.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	next();
             }
        });
		
		btnInfo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	info();
             }
        });
		
		restore();
		calculateTimes();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		switch(item.getItemId()){
		
		case R.id.instructions:
			
			Intent myIntent = new Intent(getApplicationContext(), Instructions.class);
			startActivity(myIntent);
			break;
		}
		return true;
	}
	
	private void restore() {
		if(DataLogConfiguration.totalTime == 0) {
			rbMinutes.setChecked(true);
		}
		else {
			if(DataLogConfiguration.totalTime >= 60) {
				rbHours.setChecked(true);
				input.setText(String.valueOf(DataLogConfiguration.totalTime / 60));
			}
			else {
				rbMinutes.setChecked(true);
				input.setText(String.valueOf(DataLogConfiguration.totalTime));
			}
		}
	}
	
	private void calculateTimes() {
		float voltage = DataLogConfiguration.batteryVoltage;
		
		// First calculate plugged time
		int totalSensors = 0;
		for(int i = 0; i < DataLogConfiguration.NUM_SENSORS; i++) {
			if(DataLogConfiguration.sensorsEnabled[i] == true) {
				totalSensors++;
			}
		}
		
		int totalLogs = 125000 / totalSensors;
		totalTime = DataLogConfiguration.sampleRate*totalLogs / 3600;
		
		totalTimeBatt = 0;
		if(voltage >= 4) {
			if((DataLogConfiguration.sensorsEnabled[4] == true) || (DataLogConfiguration.sensorsEnabled[5] == true)) {
				totalTimeBatt = 5;
			}
			else {
				totalTimeBatt = 130;
			}
		}
		else if((voltage >= 3.9) && (voltage < 4)) {
			if((DataLogConfiguration.sensorsEnabled[4] == true) || (DataLogConfiguration.sensorsEnabled[5] == true)) {
				totalTimeBatt = 4;
			}
			else {
				totalTimeBatt = 90;
			}
		}
		else if((voltage >= 3.8) && (voltage < 3.9)) {
			if((DataLogConfiguration.sensorsEnabled[4] == true) || (DataLogConfiguration.sensorsEnabled[5] == true)) {
				totalTimeBatt = 3;
			}
			else {
				totalTimeBatt = 60;
			}
		}
		else if((voltage >= 3.7) && (voltage < 3.8)) {
			if((DataLogConfiguration.sensorsEnabled[4] == true) || (DataLogConfiguration.sensorsEnabled[5] == true)) {
				totalTimeBatt = 2;
			}
			else {
				totalTimeBatt = 30;
			}
		}
		else if((voltage >= 3.6) && (voltage < 3.7)) {
			if((DataLogConfiguration.sensorsEnabled[4] == true) || (DataLogConfiguration.sensorsEnabled[5] == true)) {
				totalTimeBatt = 1;
			}
			else {
				totalTimeBatt = 5;
			}
		}
		
		if(totalTime < totalTimeBatt) {
			totalTimeBatt = totalTime;
		}
		
		String sVoltage = String.format("%.2f", DataLogConfiguration.batteryVoltage);
		
		tvBattTime.setText(Html.fromHtml("<b>" + totalTimeBatt + " hours</b> on current battery level (" + sVoltage + " V) if left unplugged"));
		tvPluggedTime.setText(Html.fromHtml("<b>" + totalTime + " hours</b>"));
		
		// Check for same time
		if(totalTimeBatt == totalTime) {
			tvBattTime.setVisibility(View.INVISIBLE);
			tvOr.setVisibility(View.INVISIBLE);
		}
		else {
			tvBattTime.setVisibility(View.VISIBLE);
			tvOr.setVisibility(View.VISIBLE);
		}
	}
	
	public void onRadioButtonClicked(View view) {
	    // Is the view now checked?
	    boolean checked = ((RadioButton) view).isChecked();
	    
	    // Check which checkbox was clicked
	    switch(view.getId()) {
	        case R.id.minutes:
	            if (checked) {
	            	minutes = true;
	            }
	            break;
	        case R.id.hours:
	            if (checked) {
	                minutes = false;
	            }
	            break;
	    }
	}
	
	private void back() {
		Intent myIntent = new Intent(getApplicationContext(), SampleInterval.class);
		startActivity(myIntent);
		this.finish();
	}
	
	private void info() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(R.string.time_info)
		       .setCancelable(false)
		       .setPositiveButton("OK", new DialogInterface.OnClickListener() {
		           public void onClick(DialogInterface dialog, int id) {
		           }
		       });
		AlertDialog alert = builder.create();
		alert.show();
	}
	
	private void next() {
		if(input.getText().toString().matches("")) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage("Please enter the length of time that you would like to log.")
			       .setCancelable(false)
			       .setPositiveButton("OK", new DialogInterface.OnClickListener() {
			           public void onClick(DialogInterface dialog, int id) {
			           }
			       });
			AlertDialog alert = builder.create();
			alert.show();
		}
		else {
			time = Integer.parseInt(input.getText().toString());
			if(minutes == false) {
				time = time * 60;
			}

			// Check for time that is longer than total possible time
			if(time > (totalTime*60)) {

				String message = "";
				
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setMessage("The entered time must be less than " + totalTime + " hours for this log to fit on the Sensordrone on-board memory.")
				.setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
					}
				});
				AlertDialog alert = builder.create();
				alert.show();

			}
			else {

				totalLogs = (long)((float)(1 / (float)DataLogConfiguration.sampleRate) * (time * 60));

				DataLogConfiguration.totalTime = time;
				DataLogConfiguration.totalLogs = totalLogs;

				Log.d(TAG, "Sensors: [" + DataLogConfiguration.sensorsEnabled[0] + "] [" + DataLogConfiguration.sensorsEnabled[1] + "] [" + DataLogConfiguration.sensorsEnabled[2] + "] [" + DataLogConfiguration.sensorsEnabled[3] + "] [" + DataLogConfiguration.sensorsEnabled[4] + "] [" + DataLogConfiguration.sensorsEnabled[5] + "] [" + DataLogConfiguration.sensorsEnabled[6] + "] [" + DataLogConfiguration.sensorsEnabled[7] + "] [" + DataLogConfiguration.sensorsEnabled[8] + "] [" + DataLogConfiguration.sensorsEnabled[9] + "] [" + DataLogConfiguration.sensorsEnabled[10] + "] [" + DataLogConfiguration.sensorsEnabled[11] + "] [" + DataLogConfiguration.sensorsEnabled[12] + "] [" + DataLogConfiguration.sensorsEnabled[13] + "]");
				Log.d(TAG, "Interval: " + DataLogConfiguration.sampleRate);
				Log.d(TAG, "Time: " + DataLogConfiguration.totalTime);
				Log.d(TAG, "Logs: " + DataLogConfiguration.totalLogs);

				Intent myIntent = new Intent(getApplicationContext(), Summary.class);
				startActivity(myIntent);
				this.finish();
			}
		}
	}
}
