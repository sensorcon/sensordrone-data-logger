package com.sensorcon.sensordronedatalogger;

import java.text.DecimalFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import com.androidplot.xy.BoundaryMode;
import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.XYSeries;
import com.androidplot.Plot;
import com.sensorcon.dataloggerutilities.ViewConfiguration;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PointF;
import android.os.Bundle;
import android.util.FloatMath;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;

import java.text.*;

/**
 * Creates graph from log values
 * 
 * @author Sensorcon, Inc.
 * @version 1.0.0 
 */
public class GraphData extends Activity implements View.OnTouchListener {
	
	private XYPlot mySimpleXYPlot;
    private SimpleXYSeries series = null;
    private PointF minXY;
    private PointF maxXY;

    private String title;
    private String yAxis;
    Number[] xBundleData;
    Number[] yBundleData;
    
    private ProgressDialog progressBarQuery;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.graph_layout);
        
        mySimpleXYPlot = (XYPlot) findViewById(R.id.mySimpleXYPlot);
        progressBarQuery = new ProgressDialog(this);
        progressBarQuery.setCancelable(false);
		progressBarQuery.setMessage("Generating Graph...");
		progressBarQuery.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progressBarQuery.setIndeterminate(true);

        progressBarQuery.show();
        
        makeGraph();
    }
    
    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		switch(item.getItemId()){
		
		case R.id.instructions:
			
			Intent myIntent = new Intent(getApplicationContext(), Instructions.class);
			startActivity(myIntent);
			break;
		}
		return true;
	}
	
    /**
     * Creates graph
     */
    private void makeGraph() {

    	new Thread(new Runnable() {
    		public void run() {

    			xBundleData = new Number[ViewConfiguration.queryList.size()];
    			yBundleData = new Number[ViewConfiguration.queryList.size()];

    			for(int i = 0; i < ViewConfiguration.queryList.size(); i++) {
    				xBundleData[i] = ViewConfiguration.queryList.get(i).getTimeStamp()*1000;
    				yBundleData[i] = ViewConfiguration.queryList.get(i).getValue();
    			}

    			getGraphLabels();

    			mySimpleXYPlot.setTitle(title);
    			series = new SimpleXYSeries("");
    			mySimpleXYPlot.setRangeLabel(yAxis);
    			mySimpleXYPlot.setDomainLabel("Time (hour:minute)");

    			mySimpleXYPlot.getGraphWidget().setTicksPerRangeLabel(2);
    			mySimpleXYPlot.getGraphWidget().setTicksPerDomainLabel(2);
    			mySimpleXYPlot.getGraphWidget().getBackgroundPaint().setColor(Color.TRANSPARENT);

    			// Adjust the left margin based on the number of digits to display
    			float rangeMargin;
    			float rangeMax = maxValue(yBundleData);

    			if (rangeMax < 10) {
    				mySimpleXYPlot.getGraphWidget().setRangeValueFormat(
    						new DecimalFormat("#######.##"));
    			}
    			else if (rangeMax < 1000) {
    				mySimpleXYPlot.getGraphWidget().setRangeValueFormat(
    						new DecimalFormat("#######.#"));
    			}
    			else {
    				mySimpleXYPlot.getGraphWidget().setRangeValueFormat(
    						new DecimalFormat("#######"));
    			}

    			if (rangeMax < 1e2) {
    				rangeMargin = 25;
    			}
    			else if (rangeMax < 1e3) {
    				rangeMargin = 35;
    			}
    			else if (rangeMax < 1e4) {
    				rangeMargin = 45;
    			}
    			else if (rangeMax < 1e5) {
    				rangeMargin = 55;
    			}
    			else {
    				rangeMargin = 65;
    			}
    			// Adjust the margins
    			mySimpleXYPlot.getGraphWidget().setPaddingLeft(rangeMargin); // THIS ONE FOR UNIT/NUMBER SPACING

    			mySimpleXYPlot.getGraphWidget().setRangeLabelWidth(25);
    			mySimpleXYPlot.setBorderStyle(Plot.BorderStyle.NONE, null, null);

    			// We want TIME
    			mySimpleXYPlot.getGraphWidget().setDomainValueFormat(new Format() {

    				private SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");

    				@Override
    				public StringBuffer format(Object object, StringBuffer buffer, FieldPosition field) {
    					long timestamp = ((Number) object).longValue();
    					Date date = new Date(timestamp);
    					return dateFormat.format(date, buffer, field);
    				}

    				@Override
    				public Object parseObject(String string, ParsePosition position) {
    					return null;
    				}
    			});

    			//populateSeries(series, 5);
    			series = new SimpleXYSeries(
    					Arrays.asList(xBundleData),
    					Arrays.asList(yBundleData),"");

    			int pointRed = 0;
    			int pointGreen = 200;
    			int pointBlue = 0;
    			int fillRed = 200;
    			int fillGreen = 200;
    			int fillBlue = 200;
    			mySimpleXYPlot.addSeries(series,
    					new LineAndPointFormatter(
    							Color.rgb(pointRed, pointGreen, pointBlue),
    							Color.rgb(pointRed, pointGreen, pointBlue),
    							Color.argb(175, fillRed, fillGreen, fillBlue),
    							null));

    			mySimpleXYPlot.redraw();
    			mySimpleXYPlot.calculateMinMaxVals();

    			minXY = new PointF(mySimpleXYPlot.getCalculatedMinX().floatValue(),
    					mySimpleXYPlot.getCalculatedMinY().floatValue());
    			maxXY = new PointF(mySimpleXYPlot.getCalculatedMaxX().floatValue(),
    					mySimpleXYPlot.getCalculatedMaxY().floatValue());

    			progressBarQuery.dismiss();

    		}
    	}).start();
    }
    
    private void populateSeries(SimpleXYSeries series, int max) {
        for (int i = 0; i < xBundleData.length; i++) {
            series.addLast(xBundleData[i], yBundleData[i]);
        }
    }
    
    /**
     * Sets the labels
     */
    private void getGraphLabels() {
    	
    	int id = ViewConfiguration.sensorID;
    	
    	if(id == SensorID.AMBIENT_TEMPERATURE) {
    		title = "Ambient Temperature";
    		yAxis = "\u00b0F";
    	}
    	else if(id == SensorID.HUMIDITY) {
    		title = "Humidity";
    		yAxis = "Percentage";
    	}
    	else if(id == SensorID.PRESSURE) {
    		title = "Pressure";
    		yAxis = "Pa";
    	}
    	else if(id == SensorID.PRECISION_GAS) {
    		title = "Precision Gas";
    		yAxis = "PPM of CO";
    	}
    	else if(id == SensorID.REDUCING_GAS) {
    		title = "Reducing Gas";
    		yAxis = "Ohms";
    	}
    	else if(id == SensorID.OXIDIZING_GAS) {
    		title = "Oxidizing Gas";
    		yAxis = "Ohms";
    	}
    	else if(id == SensorID.IR_TEMPERATURE) {
    		title = "IR Temperature";
    		yAxis = "\u00b0F";
    	}
    	else if(id == SensorID.CAPACITANCE) {
    		title = "Proximity Capacitance";
    		yAxis = "fF";
    	}
    	else if(id == SensorID.EXTERNAL_ADC) {
    		title = "External ADC";
    		yAxis = "V";
    	}
    	else if(id == SensorID.RED_CHANNEL) {
    		title = "Red Color";
    		yAxis = "";
    	}
    	else if(id == SensorID.GREEN_CHANNEL) {
    		title = "Green Color";
    		yAxis = "";
    	}
    	else if(id == SensorID.BLUE_CHANNEL) {
    		title = "Blue Color";
    		yAxis = "";
    	}
    	else if(id == SensorID.CLEAR_CHANNEL) {
    		title = "Clear Color";
    		yAxis = "";
    	}
    }
    
    // Definition of the touch states
    static final int NONE = 0;
    static final int ONE_FINGER_DRAG = 1;
    static final int TWO_FINGERS_DRAG = 2;
    int mode = NONE;

    PointF firstFinger;
    float distBetweenFingers;
    boolean stopThread = false;

    @Override
    public boolean onTouch(View arg0, MotionEvent event) {
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN: // Start gesture
//                firstFinger = new PointF(event.getX(), event.getY());
//                mode = ONE_FINGER_DRAG;
//                stopThread = true;
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP:
//                mode = NONE;
                break;
            case MotionEvent.ACTION_POINTER_DOWN: // second finger
//                distBetweenFingers = spacing(event);
//                // the distance check is done to avoid false alarms
//                if (distBetweenFingers > 5f) {
//                    mode = TWO_FINGERS_DRAG;
//                }
                break;
            case MotionEvent.ACTION_MOVE:
//                if (mode == ONE_FINGER_DRAG) {
//                    PointF oldFirstFinger = firstFinger;
//                    firstFinger = new PointF(event.getX(), event.getY());
//                    scroll(oldFirstFinger.x - firstFinger.x);
//                    mySimpleXYPlot.setDomainBoundaries(minXY.x, maxXY.x,
//                            BoundaryMode.FIXED);
//                    mySimpleXYPlot.redraw();
//                } else if (mode == TWO_FINGERS_DRAG) {
//                    float oldDist = distBetweenFingers;
//                    distBetweenFingers = spacing(event);
//                    zoom(oldDist / distBetweenFingers);
//                    mySimpleXYPlot.setDomainBoundaries(minXY.x, maxXY.x,
//                            BoundaryMode.FIXED);
//                    mySimpleXYPlot.redraw();
//                }
                break;
        }
        return true;
    }

    private void zoom(float scale) {
        float domainSpan = maxXY.x - minXY.x;
        float domainMidPoint = maxXY.x - domainSpan / 2.0f;
        float offset = domainSpan * scale / 2.0f;

        minXY.x = domainMidPoint - offset;
        maxXY.x = domainMidPoint + offset;

        minXY.x = Math.min(minXY.x, series.getX(series.size() - 3)
                .floatValue());
        maxXY.x = Math.max(maxXY.x, series.getX(1).floatValue());
        clampToDomainBounds(domainSpan);
    }

    private void scroll(float pan) {
        float domainSpan = maxXY.x - minXY.x;
        float step = domainSpan / mySimpleXYPlot.getWidth();
        float offset = pan * step;
        minXY.x = minXY.x + offset;
        maxXY.x = maxXY.x + offset;
        clampToDomainBounds(domainSpan);
    }

    private void clampToDomainBounds(float domainSpan) {
        float leftBoundary = series.getX(0).floatValue();
        float rightBoundary = series.getX(series.size() - 1).floatValue();
        // enforce left scroll boundary:
        if (minXY.x < leftBoundary) {
            minXY.x = leftBoundary;
            maxXY.x = leftBoundary + domainSpan;
        } else if (maxXY.x > series.getX(series.size() - 1).floatValue()) {
            maxXY.x = rightBoundary;
            minXY.x = rightBoundary - domainSpan;
        }
    }

    private float spacing(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return FloatMath.sqrt(x * x + y * y);
    }

    private float maxValue(Number[] rangeData) {
        float max = (Float) rangeData[0]; // Start with something
        for (int i = 0; i < rangeData.length; i++) {
            if ((Float)rangeData[i] > max) {
                max = (Float)rangeData[i];
            }
        }
        return max;
    }

}
