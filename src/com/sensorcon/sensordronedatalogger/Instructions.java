package com.sensorcon.sensordronedatalogger;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.webkit.WebView;

public class Instructions extends Activity {
	
	private WebView myWebView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.instructions);
		
		myWebView = (WebView)findViewById(R.id.webView1);
		
		myWebView.loadUrl("file:///android_asset/instructions.html");
	}
	
	@Override
    public void onDestroy() {
        super.onDestroy();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
}
