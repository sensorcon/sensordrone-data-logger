package com.sensorcon.sensordronedatalogger;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import com.sensorcon.dataloggerutilities.DataLogConfiguration;
import com.sensorcon.dataloggerutilities.DataLogUtility;
import com.sensorcon.sensordrone.DroneEventListener;
import com.sensorcon.sensordrone.DroneEventObject;
import com.sensorcon.sensordrone.DroneStatusListener;
import com.sensorcon.sensordrone.android.Drone;
import com.sensorcon.sensordrone.android.tools.DroneConnectionHelper;
import com.sensorcon.sensordrone.android.tools.DroneStreamer;

public class Summary extends Activity {
	
	private ImageButton btnBack;
	private ImageButton btnBegin;
	private TextView tvSensorsEnabled;
	private TextView tvSampleInterval;
	private TextView tvTotalTime;
	
	private Drone myDrone;
	private DroneConnectionHelper droneHelper;
	private DroneEventListener deListener;
	private DroneStatusListener dsListener;
	
	Intent alarmService;
	AlertDialog.Builder builder;
	
	byte[] packet;
	
	private boolean ledToggle = true;
	private DroneStreamer myBlinker;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.summary);
		
		btnBack = (ImageButton)findViewById(R.id.btnBack);
		btnBegin = (ImageButton)findViewById(R.id.btnBegin);
		tvSensorsEnabled = (TextView)findViewById(R.id.tvSensorsEnabled);
		tvSampleInterval = (TextView)findViewById(R.id.tvSampleInterval);
		tvTotalTime = (TextView)findViewById(R.id.tvTotalTime);
		builder = new AlertDialog.Builder(this);
		
		tvSensorsEnabled.setText(enabledSensorsText());
		tvSampleInterval.setText("Sample Interval: " + sampleIntervalText());
		tvTotalTime.setText("Total Time: " + totalTimeText());
		
		alarmService = new Intent(getApplicationContext(), LogAlarmService.class);
		
		btnBegin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	begin();
             }
        });
		
		btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	back();
             }
        });
		
		// Initalize drone
		myDrone = new Drone();
		droneHelper = new DroneConnectionHelper();
		
		// This will Blink our Drone, once a second, Blue
        myBlinker = new DroneStreamer(myDrone, 1000) {
            @Override
            public void repeatableTask() {
                if (ledToggle) {
                myDrone.setLEDs(0, 0, 126);
                } else {
                    myDrone.setLEDs(0,0,0);
                }
                ledToggle = !ledToggle;
            }
        };
		
        // Set up drone listeners
		deListener = new DroneEventListener() {

			@Override
			public void adcMeasured(DroneEventObject arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void altitudeMeasured(DroneEventObject arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void capacitanceMeasured(DroneEventObject arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void connectEvent(DroneEventObject arg0) {
				// Things to do when we connect to a Sensordrone
				quickMessage("Sensordrone Hardware Initialized!");
				
				// Turn on our blinker
//				myBlinker.start();
				
                myDrone.initializeDataLogger(packet);
				myDrone.disconnect();

				builder.setMessage(R.string.initialized)
				.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
		               @Override
		               public void onClick(DialogInterface dialog, int id) {
		            	   stopService(alarmService);
		                   startService(alarmService);

		                   if(!DataLogConfiguration.usingProfile) {
			                   Intent myIntent = new Intent(getApplicationContext(), Started.class);
			                   startActivity(myIntent);
		                   }
		                   
		                   kill();
		               }
		           })
		           .setNegativeButton("No", new DialogInterface.OnClickListener() {
		               public void onClick(DialogInterface dialog, int id) {

		            	   if(!DataLogConfiguration.usingProfile) {
			            	   Intent myIntent = new Intent(getApplicationContext(), Started.class);
			                   startActivity(myIntent);
		            	   }
		            	   
		            	   kill();
		               }
		           });     
				
				
		        final AlertDialog dialog = builder.create();
		        dialog.show();
			}

			@Override
			public void connectionLostEvent(DroneEventObject arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void customEvent(DroneEventObject arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void disconnectEvent(DroneEventObject arg0) {
				myBlinker.stop();
			}

			@Override
			public void humidityMeasured(DroneEventObject arg0) {}

			@Override
			public void i2cRead(DroneEventObject arg0) {}

			@Override
			public void irTemperatureMeasured(DroneEventObject arg0) {}

			@Override
			public void oxidizingGasMeasured(DroneEventObject arg0) {}

			@Override
			public void precisionGasMeasured(DroneEventObject arg0) {}

			@Override
			public void pressureMeasured(DroneEventObject arg0) {}

			@Override
			public void reducingGasMeasured(DroneEventObject arg0) {}

			@Override
			public void rgbcMeasured(DroneEventObject arg0) {}

			@Override
			public void temperatureMeasured(DroneEventObject arg0) {}

			@Override
			public void uartRead(DroneEventObject arg0) {}

			@Override
			public void unknown(DroneEventObject arg0) {}

			@Override
			public void usbUartRead(DroneEventObject arg0) {}
		};
		
		dsListener = new DroneStatusListener() {

			@Override
			public void adcStatus(DroneEventObject arg0) {}

			@Override
			public void altitudeStatus(DroneEventObject arg0) {}

			@Override
			public void batteryVoltageStatus(DroneEventObject arg0) {
				DataLogConfiguration.batteryVoltage = myDrone.batteryVoltage_Volts;
			}

			@Override
			public void capacitanceStatus(DroneEventObject arg0) {}

			@Override
			public void chargingStatus(DroneEventObject arg0) {}

			@Override
			public void customStatus(DroneEventObject arg0) {}

			@Override
			public void humidityStatus(DroneEventObject arg0) {}

			@Override
			public void irStatus(DroneEventObject arg0) {}

			@Override
			public void lowBatteryStatus(DroneEventObject arg0) {}

			@Override
			public void oxidizingGasStatus(DroneEventObject arg0) {}

			@Override
			public void precisionGasStatus(DroneEventObject arg0) {}

			@Override
			public void pressureStatus(DroneEventObject arg0) {}

			@Override
			public void reducingGasStatus(DroneEventObject arg0) {}

			@Override
			public void rgbcStatus(DroneEventObject arg0) {}

			@Override
			public void temperatureStatus(DroneEventObject arg0) {}

			@Override
			public void unknownStatus(DroneEventObject arg0) {}
		};
		
		myDrone.registerDroneListener(deListener);
		myDrone.registerDroneListener(dsListener);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		switch(item.getItemId()){
		
		case R.id.instructions:
			
			Intent myIntent = new Intent(getApplicationContext(), Instructions.class);
			startActivity(myIntent);
			break;
		}
		return true;
	}

	private String enabledSensorsText() {
		String s = "";
		
		boolean first = true;
		
		if(DataLogConfiguration.sensorsEnabled[0] == true) {
			if(first == true) {
				first = false;
			}
			else {
				s += ", ";
			}
			
			s += "Ambient Temperature";
		}
		if(DataLogConfiguration.sensorsEnabled[1] == true) {
			if(first == true) {
				first = false;
			}
			else {
				s += ", ";
			}
			
			s += "Humidity";
		}
		if(DataLogConfiguration.sensorsEnabled[2] == true) {
			if(first == true) {
				first = false;
			}
			else {
				s += ", ";
			}
			
			s += "Pressure";
		}
		if(DataLogConfiguration.sensorsEnabled[3] == true) {
			if(first == true) {
				first = false;
			}
			else {
				s += ", ";
			}
			
			s += "Precision Gas";
		}
		if(DataLogConfiguration.sensorsEnabled[4] == true) {
			if(first == true) {
				first = false;
			}
			else {
				s += ", ";
			}
			
			s += "Reducing Gas";
		}
		if(DataLogConfiguration.sensorsEnabled[5] == true) {
			if(first == true) {
				first = false;
			}
			else {
				s += ", ";
			}
			
			s += "Oxidizing Gas";
		}
		if(DataLogConfiguration.sensorsEnabled[6] == true) {
			if(first == true) {
				first = false;
			}
			else {
				s += ", ";
			}
			
			s += "IR Temperature";
		}
		if(DataLogConfiguration.sensorsEnabled[7] == true) {
			if(first == true) {
				first = false;
			}
			else {
				s += ", ";
			}
			
			s += "Capacitance";
		}
		if(DataLogConfiguration.sensorsEnabled[8] == true) {
			if(first == true) {
				first = false;
			}
			else {
				s += ", ";
			}
			
			s += "External ADC";
		}
		if(DataLogConfiguration.sensorsEnabled[9] == true) {
			if(first == true) {
				first = false;
			}
			else {
				s += ", ";
			}
			
			s += "Red Filter";
		}
		if(DataLogConfiguration.sensorsEnabled[10] == true) {
			if(first == true) {
				first = false;
			}
			else {
				s += ", ";
			}
			
			s += "Green Filter";
		}
		if(DataLogConfiguration.sensorsEnabled[11] == true) {
			if(first == true) {
				first = false;
			}
			else {
				s += ", ";
			}
			
			s += "Blue Filter";
		}
		if(DataLogConfiguration.sensorsEnabled[12] == true) {
			if(first == true) {
				first = false;
			}
			else {
				s += ", ";
			}
			
			s += "Clear Filter";
		}
		if(DataLogConfiguration.sensorsEnabled[13] == true) {
			if(first == true) {
				first = false;
			}
			else {
				s += ", ";
			}
			
			s += "External Module";
		}
		
		return s;
	}
	
	private String sampleIntervalText() {
		String s = "";
		
		int interval = DataLogConfiguration.sampleRate;
		
		if((interval >= 60) && (interval < 3600)) {
			s = Integer.toString(interval / 60) + " Minute(s)";
		}
		else if(interval == 3600) {
			s = "1 Hour";
		}
		else if(interval < 60) {
			s = Integer.toString(interval) + " Second(s)";
		}
		
		return s;
	}
	
	private String totalTimeText() {
		String s = "";
		
		long time = DataLogConfiguration.totalTime;
		
		if(time >= 60) {
			s = Long.toString(time / 60) + " Hour(s)";
		}
		else {
			s = Long.toString(time) + " Minute(s)";
		}
		
		return s;
	}
	
	private void begin() {
		
        DataLogUtility dlu = new DataLogUtility();
        
        if(DataLogConfiguration.usingProfile == true) {
        	long totalLogs = (long)((float)(1 / (float)DataLogConfiguration.sampleRate) * (DataLogConfiguration.totalTime * 60));
			DataLogConfiguration.totalLogs = totalLogs;
        }
        
		packet = dlu.createInitPacket();
		
		connect();
	}
	
	private void back() {
		Intent myIntent = new Intent(getApplicationContext(), TotalTime.class);
		startActivity(myIntent);
		this.finish();
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		doOnDisconnect();	
	}
	
	/**
	 * Things to do when drone is disconnected
	 */
	public void doOnDisconnect() {

		// Shut off any sensors that are on
		this.runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				
				// Only try and disconnect if already connected
				if (myDrone.isConnected) {
					myDrone.disconnect();
				}
			}
		});
	}
	
	public void quickMessage(final String msg) {
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT)
						.show();
			}
		});
	}
	
	public void connect() {
		Log.d("chris", "MAC: " + DataLogConfiguration.macAddress);
		Log.d("chris", "Voltage: " + DataLogConfiguration.batteryVoltage);
		myDrone.btConnect(DataLogConfiguration.macAddress);
	}
	
	private void kill() {
		this.finish();
	}
}