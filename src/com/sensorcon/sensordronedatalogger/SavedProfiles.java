package com.sensorcon.sensordronedatalogger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.j256.ormlite.android.apptools.OrmLiteBaseActivity;
import com.sensorcon.dataloggerutilities.DataLogConfiguration;
import com.sensorcon.dataloggerutilities.DatabaseHelper;
import com.sensorcon.dataloggerutilities.ProfileEntry;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class SavedProfiles extends OrmLiteBaseActivity<DatabaseHelper> {
	
	private ImageButton btnBack;
	AlertDialog.Builder builder;
	
	SimpleCursorAdapter mAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.saved_profiles);
		
		btnBack = (ImageButton)findViewById(R.id.btnBack);
		builder = new AlertDialog.Builder(this);
		
		btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	back();
             }
        });
		
		final ListView listview = (ListView) findViewById(R.id.savedProfilesList);
	   
		List<ProfileEntry> list = getHelper().getProfileRuntimeDao().queryForAll();
		final ProfileEntry[] pList = (ProfileEntry[]) list.toArray(new ProfileEntry[list.size()]);
	    
	    final ProfileArrayAdapter adapter = new ProfileArrayAdapter(this, pList);
	   
	    listview.setAdapter(adapter);

	    listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

	      @Override
	      public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
	        
	        ProfileEntry p = pList[position];
	        
	        DataLogConfiguration.sensorsEnabled[0] = p.getAmbientTempEnabled();
	        DataLogConfiguration.sensorsEnabled[1] = p.getHumidityEnabled();
	        DataLogConfiguration.sensorsEnabled[2] = p.getPressureEnabled();
	        DataLogConfiguration.sensorsEnabled[3] = p.getPrecisionGasEnabled();
	        DataLogConfiguration.sensorsEnabled[4] = p.getReducingGasEnabled();
	        DataLogConfiguration.sensorsEnabled[5] = p.getOxidizingGasEnabled();
	        DataLogConfiguration.sensorsEnabled[6] = p.getIrTempEnabled();
	        DataLogConfiguration.sensorsEnabled[7] = p.getCapacitanceEnabled();
	        DataLogConfiguration.sensorsEnabled[8] = p.getExternalADCEnabled();
	        DataLogConfiguration.sensorsEnabled[9] = p.getRedEnabled();
	        DataLogConfiguration.sensorsEnabled[10] = p.getGreenEnabled();
	        DataLogConfiguration.sensorsEnabled[11] = p.getBlueEnabled();
	        DataLogConfiguration.sensorsEnabled[12] = p.getClearEnabled();
	        
	        DataLogConfiguration.sampleRate = p.getRate();
	        DataLogConfiguration.totalTime = p.getTime();
	        
	        Intent myIntent = new Intent(getApplicationContext(), Summary.class);
	        startActivity(myIntent);
	        kill();
	      }

	    });
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		switch(item.getItemId()){
		
		case R.id.instructions:
			
			Intent myIntent = new Intent(getApplicationContext(), Instructions.class);
			startActivity(myIntent);
			break;
		}
		return true;
	}
	
	private class ProfileArrayAdapter extends ArrayAdapter<ProfileEntry> {
		private final Context context;
		private final ProfileEntry[] profiles;

	    public ProfileArrayAdapter(Context context, ProfileEntry[] profiles) {
	      super(context, R.layout.list_layout, profiles);
	      this.context = context;
	      this.profiles = profiles;
	    }

	    @Override
		public View getView(int position, View convertView, ViewGroup parent) {
	    	
	    	LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    	
	    	View rowView = inflater.inflate(R.layout.list_layout, parent, false);
	    	TextView tv = (TextView)rowView.findViewById(R.id.tv_profile_name);
	    	Button btnOpen = (Button)rowView.findViewById(R.id.btn_open);
	    	Button btnDelete = (Button)rowView.findViewById(R.id.btn_delete);
	    	tv.setText(profiles[position].getName());
	    	
	    	final int pos = position;
	    	
	    	btnOpen.setOnClickListener(new View.OnClickListener() {
	    		public void onClick(View v) {
	    			runOnUiThread(new Runnable() {
	    				@Override
	    				public void run() {
	    					ProfileEntry p = profiles[pos];
	    			        
	    			        DataLogConfiguration.sensorsEnabled[0] = p.getAmbientTempEnabled();
	    			        DataLogConfiguration.sensorsEnabled[1] = p.getHumidityEnabled();
	    			        DataLogConfiguration.sensorsEnabled[2] = p.getPressureEnabled();
	    			        DataLogConfiguration.sensorsEnabled[3] = p.getPrecisionGasEnabled();
	    			        DataLogConfiguration.sensorsEnabled[4] = p.getReducingGasEnabled();
	    			        DataLogConfiguration.sensorsEnabled[5] = p.getOxidizingGasEnabled();
	    			        DataLogConfiguration.sensorsEnabled[6] = p.getIrTempEnabled();
	    			        DataLogConfiguration.sensorsEnabled[7] = p.getCapacitanceEnabled();
	    			        DataLogConfiguration.sensorsEnabled[8] = p.getExternalADCEnabled();
	    			        DataLogConfiguration.sensorsEnabled[9] = p.getRedEnabled();
	    			        DataLogConfiguration.sensorsEnabled[10] = p.getGreenEnabled();
	    			        DataLogConfiguration.sensorsEnabled[11] = p.getBlueEnabled();
	    			        DataLogConfiguration.sensorsEnabled[12] = p.getClearEnabled();
	    			        
	    			        DataLogConfiguration.sampleRate = p.getRate();
	    			        DataLogConfiguration.totalTime = p.getTime();
	    			        
	    			        DataLogConfiguration.usingProfile = true;
	    			        
	    			        Intent myIntent = new Intent(getApplicationContext(), Summary.class);
	    			        startActivity(myIntent);
	    			        kill();
	    				}
	    			});
	    		}
	    	});
	    	
	    	btnDelete.setOnClickListener(new View.OnClickListener() {
	    		public void onClick(View v) {
	    			runOnUiThread(new Runnable() {
	    				@Override
	    				public void run() {
	    					builder.setMessage("Permanently delete " + profiles[pos].getName() + "?")
	    					.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
	    						@Override
	    						public void onClick(DialogInterface dialog, int id) {
	    							delete(profiles[pos]);
	    							update();
	    						}
	    					})
	    					.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
	    						public void onClick(DialogInterface dialog, int id) {
	    						}
	    					});
	    					
	    					final AlertDialog dialog = builder.create();
	    			        dialog.show();
	    				}
	    			});
	    		}
	    	});
	    	
	    	return rowView;
	    }
	   
	  }
	
	private void delete(ProfileEntry profile) {
		getHelper().getProfileRuntimeDao().delete(profile);
	}

	private void back() {
		Intent myIntent = new Intent(getApplicationContext(), Profiles.class);
        startActivity(myIntent);
		this.finish();
	}
	
	private void kill() {
		this.finish();
	}
	
	private void update() {
    	this.finish();
    	Intent myIntent = new Intent(getApplicationContext(), SavedProfiles.class);
        startActivity(myIntent);
    }
}