package com.sensorcon.sensordronedatalogger;

import com.sensorcon.dataloggerutilities.DataLogConfiguration;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RadioButton;

public class SampleInterval extends Activity {
	
	private ImageButton btnBack;
	private ImageButton btnNext;
	private ImageButton btnInfo;
	private RadioButton rb1;
	private RadioButton rb2;
	private RadioButton rb3;
	private RadioButton rb4;
	private RadioButton rb5;
	private RadioButton rb6;
	private RadioButton rb7;
	private RadioButton rb8;
	private RadioButton rb9;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sample_interval);
		
		btnBack = (ImageButton)findViewById(R.id.btnBack);
		btnNext = (ImageButton)findViewById(R.id.btnNext);
		btnInfo = (ImageButton)findViewById(R.id.btnInfo);
		rb1 = (RadioButton)findViewById(R.id.ti_1);
		rb2 = (RadioButton)findViewById(R.id.ti_2);
		rb3 = (RadioButton)findViewById(R.id.ti_3);
		rb4 = (RadioButton)findViewById(R.id.ti_4);
		rb5 = (RadioButton)findViewById(R.id.ti_5);
		rb6 = (RadioButton)findViewById(R.id.ti_6);
		rb7 = (RadioButton)findViewById(R.id.ti_7);
		rb8 = (RadioButton)findViewById(R.id.ti_8);
		rb9 = (RadioButton)findViewById(R.id.ti_9);
		
		btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	back();
             }
        });
		
		btnNext.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	next();
             }
        });
		
		btnInfo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	info();
             }
        });
		
		limit();
		restore();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		switch(item.getItemId()){
		
		case R.id.instructions:
			
			Intent myIntent = new Intent(getApplicationContext(), Instructions.class);
			startActivity(myIntent);
			break;
		}
		return true;
	}
	
	private void info() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(R.string.sample_info)
		       .setCancelable(false)
		       .setPositiveButton("OK", new DialogInterface.OnClickListener() {
		           public void onClick(DialogInterface dialog, int id) {
		           }
		       });
		AlertDialog alert = builder.create();
		alert.show();
	}
	
	private void restore() {
		if(DataLogConfiguration.sampleRate == 0) {
			DataLogConfiguration.sampleRate = 1;
			rb1.setChecked(true);
		}
		else if(DataLogConfiguration.sampleRate == 1) {
			rb1.setChecked(true);
		}
		else if(DataLogConfiguration.sampleRate == 5) {
			rb2.setChecked(true);
		}
		else if(DataLogConfiguration.sampleRate == 10) {
			rb3.setChecked(true);
		}
		else if(DataLogConfiguration.sampleRate == 30) {
			rb4.setChecked(true);
		}
		else if(DataLogConfiguration.sampleRate == 60) {
			rb5.setChecked(true);
		}
		else if(DataLogConfiguration.sampleRate == 60*5) {
			rb6.setChecked(true);
		}
		else if(DataLogConfiguration.sampleRate == 60*10) {
			rb7.setChecked(true);
		}
		else if(DataLogConfiguration.sampleRate == 60*30) {
			rb8.setChecked(true);
		}
		else if(DataLogConfiguration.sampleRate == 60*60) {
			rb9.setChecked(true);
		}
		else {
			rb1.setChecked(true);
		}
	}
	
	private void limit() {
		
		// RED OX limitiations
		if((DataLogConfiguration.sensorsEnabled[4] == true) || (DataLogConfiguration.sensorsEnabled[5] == true)) {
			rb1.setEnabled(false);
			rb2.setEnabled(false);
			rb3.setEnabled(false);
			rb4.setEnabled(false);
			
			rb5.setChecked(true);
			
			DataLogConfiguration.sampleRate = 60;	
		}
		else if(((DataLogConfiguration.sensorsEnabled[6] == true) || (DataLogConfiguration.sensorsEnabled[9] == true) || (DataLogConfiguration.sensorsEnabled[10] == true) || (DataLogConfiguration.sensorsEnabled[11] == true) || (DataLogConfiguration.sensorsEnabled[12] == true))) {
			rb1.setEnabled(false);
			rb2.setEnabled(false);
			
			rb3.setChecked(true);
			
			DataLogConfiguration.sampleRate = 10;	
		}
		else {
			rb1.setEnabled(true);
			rb2.setEnabled(true);
			rb3.setEnabled(true);
			rb4.setEnabled(true);
			
			rb1.setChecked(true);
		}
	}
	
	public void onRadioButtonClicked(View view) {
	    // Is the view now checked?
	    boolean checked = ((RadioButton) view).isChecked();
	    
	    // Check which checkbox was clicked
	    switch(view.getId()) {
	        case R.id.ti_1:
	            if (checked) {
	            	DataLogConfiguration.sampleRate = 1;
	            }
	            break;
	        case R.id.ti_2:
	            if (checked) {
	            	DataLogConfiguration.sampleRate = 5;
	            }
	            break;
	        case R.id.ti_3:
	            if (checked) {
	            	DataLogConfiguration.sampleRate = 10;
	            }
	            break;
	        case R.id.ti_4:
	            if (checked) {
	            	DataLogConfiguration.sampleRate = 30;
	            }
	            break;
	        case R.id.ti_5:
	            if (checked) {
	            	DataLogConfiguration.sampleRate = 60;
	            }
	            break;
	        case R.id.ti_6:
	            if (checked) {
	            	DataLogConfiguration.sampleRate = 60*5;
	            }
	            break;
	        case R.id.ti_7:
	            if (checked) {
	            	DataLogConfiguration.sampleRate = 60*10;
	            }
	            break;
	        case R.id.ti_8:
	            if (checked) {
	            	DataLogConfiguration.sampleRate = 60*30;
	            }
	            break;
	        case R.id.ti_9:
	            if (checked) {
	            	DataLogConfiguration.sampleRate = 60*60;
	            }
	            break;
	    }
	}

	private void back() {
		Intent myIntent = new Intent(getApplicationContext(), ChooseSensors.class);
		startActivity(myIntent);
		this.finish();
	}
	
	private void next() {
		Intent myIntent = new Intent(getApplicationContext(), TotalTime.class);
		startActivity(myIntent);
		this.finish();
	}
}
