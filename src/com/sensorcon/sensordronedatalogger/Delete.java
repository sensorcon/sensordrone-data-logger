package com.sensorcon.sensordronedatalogger;

import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.Callable;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.android.apptools.OrmLiteBaseActivity;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.table.TableUtils;
import com.sensorcon.dataloggerutilities.DatabaseEntry;
import com.sensorcon.dataloggerutilities.DatabaseHelper;
import com.sensorcon.dataloggerutilities.MACEntry;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

/**
 * Deletes entire entry database
 * 
 * @author Sensorcon, Inc.
 * @version 1.0.0 
 */
public class Delete extends OrmLiteBaseActivity<DatabaseHelper> {

	private boolean DEBUG = false;
	
	RuntimeExceptionDao<DatabaseEntry, Integer> myDao;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.delete);

 		AlertDialog.Builder builder = new AlertDialog.Builder(this);

 		// If in debug mode, disable delete
 		if(DEBUG) {
 			builder.setMessage("DEBUG MODE: DELETE DISABLED")
 			.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
 				@Override
 				public void onClick(DialogInterface dialog, int id) {
 					kill();
 				}
 			});
 		}
 		else {
 			builder.setMessage(R.string.delete_disclaimer)
 			.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
 				@Override
 				public void onClick(DialogInterface dialog, int id) {
 					delete();
 				}
 			})
 			.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
 				public void onClick(DialogInterface dialog, int id) {
 					kill();
 				}
 			}); 
 		}

 		final AlertDialog dialog = builder.create();
 		dialog.show();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		switch(item.getItemId()){
		
		case R.id.instructions:
			
			Intent myIntent = new Intent(getApplicationContext(), Instructions.class);
			startActivity(myIntent);
			break;
		}
		return true;
	}
	
	
	/**
	 * Deletes entry table
	 */
	private void delete() {
		
		try {
			TableUtils.dropTable(OpenHelperManager.getHelper(this, DatabaseHelper.class).getConnectionSource(), DatabaseEntry.class, true);
			TableUtils.createTable(OpenHelperManager.getHelper(this, DatabaseHelper.class).getConnectionSource(), DatabaseEntry.class);
			
			TableUtils.dropTable(OpenHelperManager.getHelper(this, DatabaseHelper.class).getConnectionSource(), MACEntry.class, true);
			TableUtils.createTable(OpenHelperManager.getHelper(this, DatabaseHelper.class).getConnectionSource(), MACEntry.class);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		quickMessage("Database Deleted");
		
		kill(); 
	}

	/**
	 * Kills activity
	 */
	private void kill() {
		this.finish();
	}
	
	public void quickMessage(final String msg) {
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT)
						.show();
			}
		});
	}
}
