package com.sensorcon.sensordronedatalogger;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.*;
import android.widget.*;
import com.j256.ormlite.dao.Dao;
import com.sensorcon.dataloggerutilities.DatabaseEntry;
import com.sensorcon.dataloggerutilities.DatabaseHelper;
import com.sensorcon.dataloggerutilities.ViewConfiguration;

import java.sql.SQLException;
import java.util.concurrent.Callable;

/**
 * Shows database entries in list view for selected sensor
 * 
 * @author Sensorcon, Inc.
 * @version 1.0.0 
 */
public class EntryList extends Activity {
	
	// UI stuff
	ListView entryList;
	EntryAdapter entryAdapter;
	private ImageButton btnBack;
	private ImageButton btnGraph;
	private ImageButton btnDelete;
	private ProgressDialog progressBar;
	
	// Database
	Dao<DatabaseEntry,Integer> dao;
	final DatabaseHelper helper = new DatabaseHelper(this);
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.entry_list);
		
		
		try {
			dao = helper.getEntryDao();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		btnBack = (ImageButton)findViewById(R.id.btnBack);
		btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	back();
             }
        });
	
		btnGraph = (ImageButton)findViewById(R.id.btnGraph);
		btnGraph.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	 Intent myIntent = new Intent(getApplicationContext(), GraphData.class);
				  startActivity(myIntent);
             }
        });
		
		btnDelete = (ImageButton)findViewById(R.id.btnDelete);
		btnDelete.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	 deleteSelected();
             }
        });
		
		// Get query
		final DatabaseEntry[] arrayList = (DatabaseEntry[])ViewConfiguration.queryList.toArray(new DatabaseEntry[ViewConfiguration.queryList.size()]);

        Log.d("ZZZ", String.format("There are %d logs here", arrayList.length));
		// Set up list view
		entryAdapter = new EntryAdapter(getApplicationContext(), arrayList);
		entryList = (ListView)findViewById(R.id.entryList);
		entryList.setAdapter(entryAdapter);
		
		entryList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

		      @Override
		      public void onItemClick(AdapterView<?> parent, final View view,
		          int position, long id) {
		      }

		    });
		
		// Only graph if at least 2 values
		if(arrayList.length < 2) {
			btnGraph.setEnabled(false);
		}

	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		switch(item.getItemId()){
		
		case R.id.instructions:
			
			Intent myIntent = new Intent(getApplicationContext(), Instructions.class);
			startActivity(myIntent);
			break;
		}
		return true;
	}
	
	/**
	 * Deletes a data set for the chosen sensor
	 */
	private void deleteSelected() {
		 progressBar = new ProgressDialog(this);
         progressBar.setCancelable(true);
         progressBar.setMessage("Deleting Logs...");
         progressBar.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
         progressBar.setProgress(0);
         progressBar.setMax(ViewConfiguration.queryList.size());
         progressBar.show();
         
         AlertDialog.Builder builder = new AlertDialog.Builder(this);
  		
  		builder.setMessage("Are you sure you want to delete these logs?")
  		.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                 @Override
                 public void onClick(DialogInterface dialog, int id) {
                	 new Thread(new Runnable() {

                       	 public void run() {

                       		 try {
								dao.callBatchTasks(new Callable<Object>() {

									 @Override
									 public Object call() throws Exception {
										 
										 int progressBarStatus = 0;
										 
										 for(DatabaseEntry de : ViewConfiguration.queryList) {
											 dao.delete(de);
											 progressBarStatus++;
											 progressBar.setProgress(progressBarStatus);
										 }

										 kill(); 

										 progressBar.dismiss();
										 return null;
									 }
								 });
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
                       	 }
                        }).start();
                 }
             })
             .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                 public void onClick(DialogInterface dialog, int id) {
                	 progressBar.dismiss();
                 }
             }); 
  		
  		final AlertDialog dialog = builder.create();
  		dialog.show();
	}
	
	/**
	 * Kill activity when back button pressed
	 */
	private void back() {
		this.finish();
	}
	
	/**
	 * Adapter to show logs in list view
	 */
	public class EntryAdapter extends ArrayAdapter<DatabaseEntry> {
		
		private final DatabaseEntry[] entryList;
		private final Context context;
		java.util.Date time;
		String timestamp;
		String value;
		
		public EntryAdapter(Context context, DatabaseEntry[] entryArray) {
			super(context, R.layout.drone_row, entryArray);
			this.entryList = entryArray;
			this.context = context;
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			
			LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View rowView = inflater.inflate(R.layout.drone_row, parent, false);
			TextView rowTV = (TextView)rowView.findViewById(R.id.tvDrone);
			rowTV.setBackgroundResource(R.drawable.rounded);
			rowTV.setTextColor(Color.BLACK);
			rowTV.setTextSize(15);
			
			time = new java.util.Date((long)entryList[position].getTimeStamp()*1000);
			
			timestamp = time.toLocaleString();
			value = String.format("%.1f",entryList[position].getValue());

			rowTV.setText(timestamp + ": " + value + " " + getUnit(entryList[position].getSensor()));
			
			return rowView;
		}
		
		private String getUnit(int sensor) {
			
			String retVal = "";
			
			if(sensor == SensorID.AMBIENT_TEMPERATURE) {
				retVal = "\u00b0F";
			}
			else if(sensor == SensorID.HUMIDITY) {
				retVal = "%";
			}
			else if(sensor == SensorID.PRESSURE) {
				retVal = "Pa";
			}
			else if(sensor == SensorID.PRECISION_GAS) {
				retVal = "PPM";
			}
			else if(sensor == SensorID.REDUCING_GAS) {
				retVal = "Ohms";
			}
			else if(sensor == SensorID.OXIDIZING_GAS) {
				retVal = "Ohms";
			}
			else if(sensor == SensorID.IR_TEMPERATURE) {
				retVal = "\u00b0F";
			}
			else if(sensor == SensorID.CAPACITANCE) {
				retVal = "fF";
			}
			else if(sensor == SensorID.EXTERNAL_ADC) {
				retVal = "V";
			}
			else if(sensor == SensorID.RED_CHANNEL) {
				retVal = "";
			}
			else if(sensor == SensorID.GREEN_CHANNEL) {
				retVal = "";
			}
			else if(sensor == SensorID.BLUE_CHANNEL) {
				retVal = "";
			}
			else if(sensor == SensorID.CLEAR_CHANNEL) {
				retVal = "";
			}
			
			return retVal;
		}
	}
	
	/**
	 * Kill the activity
	 */
	private void kill() {
		this.finish();
	}
}
