package com.sensorcon.sensordronedatalogger;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class LogAlarmService extends Service {
	
	private NotificationManager notifier;
	android.support.v4.app.NotificationCompat.Builder notifyLogComplete;
	private LogCompleteAlarm logAlarm;

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();

		logAlarm = new LogCompleteAlarm();
		logAlarm.setAlarm(getApplicationContext());
	   
	}
	
   @Override
   public int onStartCommand(Intent intent, int flags, int startId)
   {
	   return START_NOT_STICKY;
       //return super.onStartCommand(intent, flags, startId);
   }
 
    @Override
    public void onDestroy() 
    {
        super.onDestroy();
        
        logAlarm.CancelAlarm(getApplicationContext());
    }
}
